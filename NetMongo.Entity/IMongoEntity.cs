﻿namespace NetMongo.Entity
{
    public interface IMongoEntity
    {
         string Id { get; }
    }
}
