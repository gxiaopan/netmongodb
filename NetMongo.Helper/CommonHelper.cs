using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Web;
using System.Collections;
using System.Reflection;
using System.Data;
using System.IO;

namespace NetMongo.Helper
{
	public class CommonHelper
	{
		private static string[] _weekdays;

		private static Regex _tbbrRegex;

		static CommonHelper()
		{
			string[] strArrays = new string[] { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
			CommonHelper._weekdays = strArrays;
			CommonHelper._tbbrRegex = new Regex("\\s*|\\t|\\r|\\n", RegexOptions.IgnoreCase);
		}

		public CommonHelper()
		{
		}

		public static string ClearTBBR(string str)
		{
			string str1;
			str1 = (string.IsNullOrEmpty(str) ? string.Empty : CommonHelper._tbbrRegex.Replace(str, ""));
			return str1;
		}

		public static long ConvertIPToInt64(string ip)
		{
			long num = Convert.ToInt64(ip.Replace(".", string.Empty));
			return num;
		}

		public static string DeleteNullOrSpaceRow(string s)
		{
			string str;
			if (!string.IsNullOrEmpty(s))
			{
				string[] strArrays = StringHelper.SplitString("\r\n");
				StringBuilder stringBuilder = new StringBuilder();
				string[] strArrays1 = strArrays;
				for (int i = 0; i < strArrays1.Length; i++)
				{
					string str1 = strArrays1[i];
					if (!string.IsNullOrWhiteSpace(str1))
					{
						stringBuilder.AppendFormat("{0}\r\n", str1);
					}
				}
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Remove(stringBuilder.Length - 2, 2);
				}
				str = stringBuilder.ToString();
			}
			else
			{
				str = "";
			}
			return str;
		}

		public static string EscapeRegex(string s)
		{
			string[] strArrays = new string[] { "\\", ".", "+", "*", "?", "{", "}", "[", "^", "]", "$", "(", ")", "=", "!", "<", ">", "|", ":" };
			string[] strArrays1 = strArrays;
			strArrays = new string[] { "\\\\", "\\.", "\\+", "\\*", "\\?", "\\{", "\\}", "\\[", "\\^", "\\]", "\\$", "\\(", "\\)", "\\=", "\\!", "\\<", "\\>", "\\|", "\\:" };
			string[] strArrays2 = strArrays;
			for (int i = 0; i < strArrays1.Length; i++)
			{
				s = s.Replace(strArrays1[i], strArrays2[i]);
			}
			return s;
		}

		public static string GetChineseDate()
		{
			return DateTime.Now.ToString("yyyy月MM日dd");
		}

		public static string GetDate()
		{
			return DateTime.Now.ToString("yyyy-MM-dd");
		}

		public static string GetDateTime()
		{
			return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
		}

		public static string GetDateTimeMS()
		{
			return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fffffff");
		}

		public static string GetDateTimeU()
		{
			return string.Format("{0:U}", DateTime.Now);
		}

		public static string GetDay()
		{
			return DateTime.Now.Day.ToString("00");
		}

		public static string GetDayOfWeek()
		{
			return DateTime.Now.DayOfWeek.ToString();
		}

		public static string GetEmailProvider(string email)
		{
			string str;
			int num = email.LastIndexOf('@');
			str = (num <= 0 ? string.Empty : email.Substring(num + 1));
			return str;
		}

		public static string GetHour()
		{
			return DateTime.Now.Hour.ToString("00");
		}

		public static string GetHtmlBS(int count)
		{
			string str;
			if (count == 1)
			{
				str = "&nbsp;&nbsp;&nbsp;&nbsp;";
			}
			else if (count == 2)
			{
				str = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			}
			else if (count != 3)
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < count; i++)
				{
					stringBuilder.Append("&nbsp;&nbsp;&nbsp;&nbsp;");
				}
				str = stringBuilder.ToString();
			}
			else
			{
				str = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			}
			return str;
		}

		public static string GetHtmlSpan(int count)
		{
			string str;
			if (count <= 0)
			{
				str = "";
			}
			else if (count == 1)
			{
				str = "<span></span>";
			}
			else if (count == 2)
			{
				str = "<span></span><span></span>";
			}
			else if (count != 3)
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < count; i++)
				{
					stringBuilder.Append("<span></span>");
				}
				str = stringBuilder.ToString();
			}
			else
			{
				str = "<span></span><span></span><span></span>";
			}
			return str;
		}

		public static int GetIndexInArray(string s, string[] array, bool ignoreCase)
		{
			int num;
			if ((string.IsNullOrEmpty(s) || array == null ? false : array.Length != 0))
			{
				int num1 = 0;
				string str = null;
				if (ignoreCase)
				{
					s = s.ToLower();
				}
				string[] strArrays = array;
				int num2 = 0;
				while (num2 < strArrays.Length)
				{
					string str1 = strArrays[num2];
					str = (!ignoreCase ? str1 : str1.ToLower());
					if (!(s == str))
					{
						num1++;
						num2++;
					}
					else
					{
						num = num1;
						return num;
					}
				}
				num = -1;
			}
			else
			{
				num = -1;
			}
			return num;
		}

		public static int GetIndexInArray(string s, string[] array)
		{
			return CommonHelper.GetIndexInArray(s, array, false);
		}

		public static string GetMonth()
		{
			return DateTime.Now.Month.ToString("00");
		}

		public static string GetTime()
		{
			return DateTime.Now.ToString("HH:mm:ss");
		}

		public static string GetUniqueString(string s)
		{
			return CommonHelper.GetUniqueString(s, ",");
		}

		public static string GetUniqueString(string s, string splitStr)
		{
			string str = CommonHelper.ObjectArrayToString(CommonHelper.RemoveRepeaterItem(StringHelper.SplitString(s, splitStr)), splitStr);
			return str;
		}

		public static string GetWeek()
		{
			return CommonHelper._weekdays[(int)DateTime.Now.DayOfWeek];
		}

		public static string GetYear()
		{
			return DateTime.Now.Year.ToString();
		}

		public static string HideEmail(string email)
		{
			string str;
			int num = email.LastIndexOf('@');
			if (num == 1)
			{
				str = string.Concat("*", email.Substring(num));
			}
			else if (num != 2)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(email.Substring(0, 2));
				for (int i = num - 2; i > 0; i--)
				{
					stringBuilder.Append("*");
				}
				stringBuilder.Append(email.Substring(num));
				str = stringBuilder.ToString();
			}
			else
			{
				str = string.Concat(email[0], "*", email.Substring(num));
			}
			return str;
		}

		public static string HideMobile(string mobile)
		{
			string str = string.Concat(mobile.Substring(0, 3), "*****", mobile.Substring(8));
			return str;
		}

		public static string IntArrayToString(int[] array, string splitStr)
		{
			string str;
			if ((array == null ? false : array.Length != 0))
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < array.Length; i++)
				{
					stringBuilder.AppendFormat("{0}{1}", array[i], splitStr);
				}
				str = stringBuilder.Remove(stringBuilder.Length - splitStr.Length, splitStr.Length).ToString();
			}
			else
			{
				str = "";
			}
			return str;
		}

		public static string IntArrayToString(int[] array)
		{
			return CommonHelper.IntArrayToString(array, ",");
		}

		public static bool IsInArray(string s, string[] array, bool ignoreCase)
		{
			return CommonHelper.GetIndexInArray(s, array, ignoreCase) > -1;
		}

		public static bool IsInArray(string s, string[] array)
		{
			return CommonHelper.IsInArray(s, array, false);
		}

		public static bool IsInArray(string s, string array, string splitStr, bool ignoreCase)
		{
			bool flag = CommonHelper.IsInArray(s, StringHelper.SplitString(array, splitStr), ignoreCase);
			return flag;
		}

		public static bool IsInArray(string s, string array, string splitStr)
		{
			bool flag = CommonHelper.IsInArray(s, StringHelper.SplitString(array, splitStr), false);
			return flag;
		}

		public static bool IsInArray(string s, string array)
		{
			bool flag = CommonHelper.IsInArray(s, StringHelper.SplitString(array, ","), false);
			return flag;
		}

		public static string ObjectArrayToString(object[] array, string splitStr)
		{
			string str;
			if ((array == null ? false : array.Length != 0))
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < array.Length; i++)
				{
					stringBuilder.AppendFormat("{0}{1}", array[i], splitStr);
				}
				str = stringBuilder.Remove(stringBuilder.Length - splitStr.Length, splitStr.Length).ToString();
			}
			else
			{
				str = "";
			}
			return str;
		}

		public static string ObjectArrayToString(object[] array)
		{
			return CommonHelper.ObjectArrayToString(array, ",");
		}

		public static string[] RemoveArrayItem(string[] array, string removeItem, bool removeBackspace, bool ignoreCase)
		{
			string[] strArrays;
			if ((array == null ? true : array.Length <= 0))
			{
				strArrays = array;
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder();
				if (ignoreCase)
				{
					removeItem = removeItem.ToLower();
				}
				string str = "";
				string[] strArrays1 = array;
				for (int i = 0; i < strArrays1.Length; i++)
				{
					string str1 = strArrays1[i];
					str = (!ignoreCase ? str1 : str1.ToLower());
					if (str != removeItem)
					{
						stringBuilder.AppendFormat("{0}_", (removeBackspace ? str1.Trim() : str1));
					}
				}
				strArrays = StringHelper.SplitString(stringBuilder.Remove(stringBuilder.Length - 1, 1).ToString(), "_");
			}
			return strArrays;
		}

		public static string[] RemoveArrayItem(string[] array)
		{
			return CommonHelper.RemoveArrayItem(array, "", true, false);
		}

		public static int[] RemoveRepeaterItem(int[] array)
		{
			int i;
			int[] numArray;
			if ((array == null ? false : array.Length >= 2))
			{
				Array.Sort<int>(array);
				int num = 1;
				for (i = 1; i < array.Length; i++)
				{
					if (array[i] != array[i - 1])
					{
						num++;
					}
				}
				int[] numArray1 = new int[num];
				numArray1[0] = array[0];
				int num1 = 1;
				for (i = 1; i < array.Length; i++)
				{
					if (array[i] != array[i - 1])
					{
						int num2 = num1;
						num1 = num2 + 1;
						numArray1[num2] = array[i];
					}
				}
				numArray = numArray1;
			}
			else
			{
				numArray = array;
			}
			return numArray;
		}

		public static string[] RemoveRepeaterItem(string[] array)
		{
			int i;
			string[] strArrays;
			if ((array == null ? false : array.Length >= 2))
			{
				Array.Sort<string>(array);
				int num = 1;
				for (i = 1; i < array.Length; i++)
				{
					if (array[i] != array[i - 1])
					{
						num++;
					}
				}
				string[] strArrays1 = new string[num];
				strArrays1[0] = array[0];
				int num1 = 1;
				for (i = 1; i < array.Length; i++)
				{
					if (array[i] != array[i - 1])
					{
						int num2 = num1;
						num1 = num2 + 1;
						strArrays1[num2] = array[i];
					}
				}
				strArrays = strArrays1;
			}
			else
			{
				strArrays = array;
			}
			return strArrays;
		}

		public static string[] RemoveStringItem(string s, string splitStr)
		{
			string[] strArrays = CommonHelper.RemoveArrayItem(StringHelper.SplitString(s, splitStr), "", true, false);
			return strArrays;
		}

		public static string[] RemoveStringItem(string s)
		{
			string[] strArrays = CommonHelper.RemoveArrayItem(StringHelper.SplitString(s), "", true, false);
			return strArrays;
		}

		public static string StringArrayToString(string[] array, string splitStr)
		{
			return CommonHelper.ObjectArrayToString(array, splitStr);
		}

		public static string StringArrayToString(string[] array)
		{
			return CommonHelper.StringArrayToString(array, ",");
		}

		public static decimal SubDecimal(decimal dec, int pointCount)
		{
			string str = dec.ToString();
			decimal num = TypeHelper.StringToDecimal(str.Substring(0, str.IndexOf('.') + pointCount + 1));
			return num;
		}

		public static string TBBRTrim(string str)
		{
			string empty;
			if (string.IsNullOrEmpty(str))
			{
				empty = string.Empty;
			}
			else
			{
				string str1 = str.Trim();
				char[] chrArray = new char[] { '\r' };
				string str2 = str1.Trim(chrArray);
				chrArray = new char[] { '\n' };
				string str3 = str2.Trim(chrArray);
				chrArray = new char[] { '\t' };
				empty = str3.Trim(chrArray);
			}
			return empty;
		}
        /// <summary>
        /// 判断object是否为List<DateTime> 如果是就转换成List<DateTime>返回
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="dtlist"></param>
        /// <returns></returns>
        public static bool IsDateTimeList(object dt, out List<DateTime> dtlist)
        {
            dtlist = new List<DateTime>();
            try
            {
                var s =new List<DateTime>();
                var dtJson = JsonHelper.ToJson(dt);
                if (JConstructor.Parse(dtJson).First.Type==JTokenType.Date)
                {
                    s.Add(DateTime.Parse(JConstructor.Parse(dtJson).First.ToString()));
                    s.Add(DateTime.Parse(JConstructor.Parse(dtJson).Last.ToString()));
                }
                if (s != null&s.Any())
                {
                    dtlist = s;
                }
                else
                {
                    dtlist = null;
                }
                return dt != null;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return false;
            }
        }
        /// <summary>
        /// 判断object是否为List<int> 如果是就转换成List<int>返回
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="doublelist"></param>
        /// <returns></returns>
        public static bool IsDoubleList(object dt, out List<Double> list)
        {
            list = new List<Double>();
            try
            {
                var s =JsonHelper.DeserializeObject<List<Double>>(dt.ToJson());
                if (s != null)
                {
                    list = s;
                }
                else
                {
                    Double[] m = dt as Double[];
                    if(m != null)
                    {
                        list = m.ToList();
                    }
                    else
                    {
                        list = null;
                    }
                }
                return dt != null;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return false;
            }
        }
        /// <summary>
        /// 判断object是否为List<DateTime> 如果是就转换成List<DateTime>返回
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="dtlist"></param>
        /// <returns></returns>
        public static bool IsStringList(object obj, out List<string> list)
        {
            list = new List<string>();
            try
            {
                var s = JsonHelper.DeserializeObject<List<string>>(obj.ToString());
                if (s != null & s.Any())
                {
                    list = s;
                }
                else
                {
                    list = null;
                }
                return list != null;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return false;
            }
        }
        /// <summary>
        /// 传入模版文件相对路径，返回复制后的临时文件相对路径
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool GetInterimOutPutFilePath(ref string filePath, string filename = "")
        {
            try
            {
                filename = string.IsNullOrEmpty(filename) ? IdentityCreator.NextIdentity : filename + DateTime.Now.ToString("HHmmss");

                filePath = HttpContext.Current.Server.MapPath(filePath);
                if (File.Exists(filePath))
                {
                    string fileExt = filePath.Substring(filePath.LastIndexOf("."));
                    //临时文件目录
                    string fpath = "/Resource/OutPut/" + DateTime.Now.ToString("yyyyMMdd") + "/";
                    string path = HttpContext.Current.Server.MapPath(fpath);
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    //特殊字符过滤
                    string interimFile = fpath + filename.ToFileName() + fileExt;
                    string interimFilePath = HttpContext.Current.Server.MapPath(interimFile);
                    File.Copy(filePath, interimFilePath, true);
                    File.SetAttributes(interimFilePath, FileAttributes.Normal);
                    filePath = interimFile;
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(filename + ":" + ex.Message);
            }
            return false;
        }
    }

    public static class Utils
    {
        public static object GetPropertyValue(this object m, string name)
        {
            return m.GetType().GetProperty(name).GetValue(m) ?? string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ToJoinStr(this List<string> text, string format = "'{0}',")
        {
            string s = string.Empty;
            foreach (object o in text)
            {
                if (o != null)
                {
                    s += string.Format(format, o.ToString());
                }
            }
            return s.TrimEnd(',');
        }
        /// <summary>
        /// 添加url地址参数
        /// </summary>
        /// <param name="text"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public static string ToConcatQueryString(this string text, string query)
        {
            bool hasConnect = text.IndexOf("?") > -1;
            if (hasConnect)
            {
                bool hasReplace = query.IndexOf("=") > -1 && text.IndexOf(query.Substring(0, query.IndexOf("=") + 1)) > -1;
                if (hasReplace)
                {
                    string qn = query.Substring(0, query.IndexOf("=") + 1);
                    string qf = text.Substring(0, text.IndexOf(qn));
                    bool hasOtherQuery = text.Substring(qf.Length).IndexOf("&") > -1;
                    string ql = hasOtherQuery ? text.Substring(qf.Length + text.Substring(qf.Length).IndexOf("&")) : "";
                    return string.Format(qf + "{0}" + ql, query);
                }
                else
                {
                    return text + "&" + query;
                }
            }
            else
            {
                return text + "?" + query;
            }
        }
        /// <summary>
        /// 在前面补充字符
        /// </summary>
        /// <param name="text"></param>
        /// <param name="l"></param>
        /// <param name="padStr"></param>
        /// <returns></returns>
        public static string ToPadBefore(this string text, int l, string padStr = " ")
        {
            string s = string.Empty;
            while (l > 0)
            {
                l--;
                s += padStr;
            }
            return s + text;
        }
        /// <summary>
        /// 将集合类转换成DataTable
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public static DataTable ToDataTable(this IList list)
        {
            DataTable result = new DataTable();
            if (list.Count > 0)
            {
                PropertyInfo[] propertys = list[0].GetType().GetProperties();
                foreach (PropertyInfo pi in propertys)
                {
                    if (pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition().GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        result.Columns.Add(pi.Name, pi.PropertyType.GetGenericArguments()[0]);
                    }
                    else
                    {
                        result.Columns.Add(pi.Name, pi.PropertyType);
                    }
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ArrayList tempList = new ArrayList();
                    foreach (PropertyInfo pi in propertys)
                    {
                        object obj = pi.GetValue(list[i], null) == null ? DBNull.Value : pi.GetValue(list[i], null);
                        tempList.Add(obj);
                    }
                    object[] array = tempList.ToArray();
                    result.LoadDataRow(array, true);
                }
            }
            return result;
        }
        /// <summary>
        /// ilist to list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gbList"></param>
        /// <returns></returns>
        public static List<T> ToCList<T>(this IList gbList) where T : class
        {
            if (gbList != null && gbList.Count >= 1)
            {
                List<T> list = new List<T>();
                PropertyInfo[] propertys = typeof(T).GetProperties();
                PropertyInfo[] propertys_1 = gbList[0].GetType().GetProperties();
                T t;
                for (int i = 0; i < gbList.Count; i++)
                {
                    t = Activator.CreateInstance<T>();
                    foreach (var item in propertys)
                    {

                        if (propertys_1.Where(a => a.Name == item.Name).Count() == 1)
                        {
                            var o = propertys_1.Where(a => a.Name == item.Name).FirstOrDefault();
                            var v = o.GetValue(gbList[i], null);
                            item.SetValue(t, v, null);
                        }
                    }
                    list.Add(t);

                }
                return list;
            }
            return null;
        }
        /// <summary>
        /// 去除html代码
        /// </summary>
        /// <param name="Htmlstring"></param>
        /// <returns></returns>
        public static string ToNoHtml(this string Htmlstring)
        {
            if (string.IsNullOrEmpty(Htmlstring))
            {
                return string.Empty;
            }

            //删除脚本
            Htmlstring = Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
            //删除HTML
            Htmlstring = Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"-->", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<!--.*", "", RegexOptions.IgnoreCase);

            Htmlstring = Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(nbsp|#160);", " ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(ndash);", "–", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(mdash);", "—", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(rdquo);", "”", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(ldquo);", "“", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&#(\d+);", "", RegexOptions.IgnoreCase);

            Htmlstring.Replace("<", "");
            Htmlstring.Replace(">", "");
            Htmlstring.Replace("\r\n", "");
            Htmlstring = HttpUtility.HtmlEncode(Htmlstring).Trim();
            return Htmlstring;
        }
        /// <summary>
        /// 在产生xml文件的时候，过滤低位非打印字符
        /// </summary>
        /// <param name="tmp"></param>
        /// <returns></returns>
        public static string ToReplaceLowOrderASCIICharacters(this string tmp)
        {
            StringBuilder info = new StringBuilder();
            foreach (char cc in tmp)
            {
                int ss = (int)cc;
                if (((ss >= 0) && (ss <= 8)) || ((ss >= 11) && (ss <= 12)) || ((ss >= 14) && (ss <= 32)))
                    info.AppendFormat(" ", ss);//&#x{0:X};
                else info.Append(cc);
            }
            return info.ToString();
        }
        /// <summary>
        /// 返回限制最长显示的字符
        /// </summary>
        /// <param name="text"></param>
        /// <param name="l"></param>
        /// <returns></returns>
        public static string ToMaxString(this string text, int l)
        {
            return CutString(text, l);
        }
        /// <summary>
        /// 按指定长度截取字符串加...
        /// </summary>
        /// <param name="text"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string CutString(string text, int length, string addText = "...")
        {
            if (string.IsNullOrEmpty(text) || length <= 0)
                return "";

            if (text.Length > length)
            {
                text = text.Substring(0, length) + addText;
            }
            return text;
        }
        public static string ToPercentString(this string text, string ptext)
        {
            try
            {
                double d = Convert.ToDouble(text);
                double dp = Convert.ToDouble(ptext);
                if (dp == 0)
                    return "0%";
                var p = d / dp;
                return p.ToString("0.00%");
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }

    public static class CustomUtility
    {
        private static readonly char[] InvalidFileNameChars = new[] { '"', '<', '>', '|', '\0', '\u0001', '\u0002', '\u0003', '\u0004', '\u0005', '\u0006', '\a', '\b', '\t', '\n', '\v', '\f', '\r', '\u000e', '\u000f', '\u0010', '\u0011', '\u0012', '\u0013', '\u0014', '\u0015', '\u0016', '\u0017', '\u0018', '\u0019', '\u001a', '\u001b', '\u001c', '\u001d', '\u001e', '\u001f', ':', '*', '?', '\\', '/' };
        public static string ToFileName(this string fileName)
        {
            return new string(fileName.ToCharArray().Where(a => !InvalidFileNameChars.Any(b => b == a)).ToArray());
        }
    }

}