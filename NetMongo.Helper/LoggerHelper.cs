﻿using System;

namespace NetMongo.Helper
{
    public sealed class IdentityCreator
    {
        /*
         * var guid = Guid.NewGuid();         
         * guid.ToString("D")   10244798-9a34-4245-b1ef-9143f9b1e68a
         * guid.ToString("N")   102447989a344245b1ef9143f9b1e68a
         * guid.ToString("B")   {10244798-9a34-4245-b1ef-9143f9b1e68a}
         * guid.ToString("P")   (10244798-9a34-4245-b1ef-9143f9b1e68a)
         * 不区另大小写
         */
        public static string NextIdentity
        {
            get
            {
                return DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + Guid.NewGuid().ToString("N").Substring(new Random().Next(0, 17), 15);
            }
        }
        public static int timestamp
        {
            get
            {
                return ((Int32)(DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds);
            }
        }
        public static string CreateOrderNo()
        {
            string randomstr = "";
            int count = 4;
            Random[] rnds = new Random[count];
            for (int i = 0; i < count; i++)
            {
                rnds[i] = new Random(unchecked((int)(DateTime.Now.Ticks >> i)));
                randomstr += rnds[i].Next(100).ToString().PadLeft(2, '0');
            }
            randomstr = randomstr.Substring(new Random().Next(0, 4), 4);
            randomstr = DateTime.Now.ToString("yyyyMMddHHmmssfffffff") + randomstr;
            return randomstr;
        }
    }

    /// <summary>
    /// 日志
    /// </summary>
    public static class Logger
    {
        private static readonly log4net.ILog logInfo = log4net.LogManager.GetLogger("CLog.Info");
        private static readonly log4net.ILog logError = log4net.LogManager.GetLogger("CLog.Error");
        /// <summary>
        /// info日志记录
        /// </summary>
        /// <param name="message"></param>
        public static void Log(string message)
        {
            if (logInfo.IsInfoEnabled)
            {
                logInfo.Info(message);
            }
        }
        /// <summary>
        /// error日志记录
        /// </summary>
        /// <param name="ex"></param>
        public static void Log(Exception ex)
        {
            if (logError.IsErrorEnabled)
            {
                if (ex.InnerException != null)
                {
                    logInfo.Info(ex.InnerException.Message);
                }
                logError.Error(ex);
            }
        }


        /// <summary>
        /// 性能日志
        /// </summary>
        private static readonly log4net.ILog logPerf = log4net.LogManager.GetLogger("Curse.Performance");
        /// <summary>
        /// 性能日志记录
        /// </summary>
        /// <param name="message"></param>
        public static void LogPerf(string message)
        {
            if (logPerf.IsInfoEnabled)
            {
                logPerf.Info(message);
            }
        }
        /// <summary>
        /// 超过指定时长的记录
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="message"></param>
        /// <param name="minsecond"></param>
        public static void LogPerf(DateTime startDate, string message, int minsecond = 3)
        {
            var ts = DateTime.Now - startDate;
            if (logPerf.IsInfoEnabled && ts.TotalSeconds > minsecond)
            {
                logPerf.Info(message + ":ts:" + ts.TotalMilliseconds);
            }
        }

    }
}
