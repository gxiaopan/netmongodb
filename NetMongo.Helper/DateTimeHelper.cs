using System;
using System.Text;

namespace NetMongo.Helper
{
	public static class DateTimeHelper
	{
        /// <summary>
        /// 时间格式yyyy-MM-ddTHH:mm:ss.fffZ   例如:2017-02-28T20:20:25.450Z
        /// </summary>
        public static string DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fffZ";

        /// <summary>
        /// 获取当前时间的总毫秒数  从1970-01-01 08：00：00 开始计算
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static long GetTicks(DateTime? dt = null)
        {
            try
            {
                DateTime nowDate = DateTime.Now;
                if (dt.HasValue)
                {
                    nowDate = dt.Value;
                }
                DateTime startTime = new DateTime(1970, 1, 1, 8, 0, 0);
                long sTick = Convert.ToInt64(nowDate.Subtract(startTime).TotalMilliseconds);
                return sTick;
            }
            catch(Exception ex)
            {
                Logger.Log(ex);
                return 0;
            }
        }

        /// <summary>
        /// 日期转换成unix时间戳
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static long DateTimeToUnixTimestamp(DateTime dateTime)
        {
            var start = new DateTime(1970, 1, 1, 0, 0, 0, dateTime.Kind);
            return Convert.ToInt64((dateTime - start).TotalSeconds);
        }

        /// <summary>
        /// unix时间戳转换成日期
        /// </summary>
        /// <param name="unixTimeStamp">时间戳（秒）</param>
        /// <returns></returns>
        public static DateTime TimestampToDateTime(this DateTime target, long timestamp)
        {
            var start = new DateTime(1970, 1, 1, 0, 0, 0, target.Kind);
            return start.AddMilliseconds(timestamp);
        }

        public static DateTime GetDateTime(long timeStamp)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
            DateTime dt = startTime.AddMilliseconds(timeStamp);
            return dt;
        }

        /// <summary>
        /// 获取当前时间的总毫秒数  从1970-01-01 08：00：00 开始计算
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static long Ticks
        {
            get {
                DateTime nowDate = DateTime.Now;
                DateTime startTime = new DateTime(1970, 1, 1, 8, 0, 0);
                long sTick = Convert.ToInt64(nowDate.Subtract(startTime).TotalMilliseconds);
                return sTick;
            }
        }
        public static DateTime GetStartDayOfWeeks(int year, int month, int index)
		{
			DateTime minValue;
			if (!(year < 1600 ? false : year <= 9999))
			{
				minValue = DateTime.MinValue;
			}
			else if (!(month < 0 ? false : month <= 12))
			{
				minValue = DateTime.MinValue;
			}
			else if (index >= 1)
			{
				DateTime dateTime = new DateTime(year, month, 1);
				int num = 7;
				if (Convert.ToInt32(dateTime.DayOfWeek.ToString("d")) > 0)
				{
					num = Convert.ToInt32(dateTime.DayOfWeek.ToString("d"));
				}
				DateTime dateTime1 = dateTime.AddDays(1 - num);
				DateTime dateTime2 = dateTime1.AddDays(index * 7);
				minValue = ((dateTime2 - dateTime.AddMonths(1)).Days <= 0 ? dateTime2 : DateTime.MinValue);
			}
			else
			{
				minValue = DateTime.MinValue;
			}
			return minValue;
		}

		public static string GetWeekSpanOfMonth(int year, int month)
		{
			string str;
			if (!(year < 1600 ? false : year <= 9999))
			{
				str = "";
			}
			else if ((month < 0 ? false : month <= 12))
			{
				StringBuilder stringBuilder = new StringBuilder();
				int num = 1;
				while (num < 5)
				{
					DateTime dateTime = new DateTime(year, month, 1);
					int num1 = 7;
					if (Convert.ToInt32(dateTime.DayOfWeek.ToString("d")) > 0)
					{
						num1 = Convert.ToInt32(dateTime.DayOfWeek.ToString("d"));
					}
					DateTime dateTime1 = dateTime.AddDays(1 - num1);
					DateTime dateTime2 = dateTime1.AddDays(num * 7);
					if ((dateTime2 - dateTime.AddMonths(1)).Days <= 0)
					{
						stringBuilder.Append(dateTime2.ToString("yyyy-MM-dd"));
						stringBuilder.Append(" ~ ");
						DateTime dateTime3 = dateTime2.AddDays(6);
						stringBuilder.Append(dateTime3.ToString("yyyy-MM-dd"));
						stringBuilder.Append(Environment.NewLine);
						num++;
					}
					else
					{
						str = "";
						return str;
					}
				}
				str = stringBuilder.ToString();
			}
			else
			{
				str = "";
			}
			return str;
		}
	}
}