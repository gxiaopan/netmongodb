using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace NetMongo.Helper
{
    public class StringHelper
	{
		public StringHelper()
		{
		}

		public static int GetStringLength(string s)
		{
			int num;
			num = (string.IsNullOrEmpty(s) ? 0 : Encoding.Default.GetBytes(s).Length);
			return num;
		}

		public static int IndexOf(string s, int order)
		{
			return StringHelper.IndexOf(s, '-', order);
		}

		public static int IndexOf(string s, char c, int order)
		{
			int num;
			int length = s.Length;
			int num1 = 0;
			while (true)
			{
				if (num1 < length)
				{
					if (c == s[num1])
					{
						if (order != 1)
						{
							order--;
						}
						else
						{
							num = num1;
							break;
						}
					}
					num1++;
				}
				else
				{
					num = -1;
					break;
				}
			}
			return num;
		}

		public static string[] SplitString(string sourceStr, string splitStr)
		{
			string[] strArrays;
			if (!(string.IsNullOrEmpty(sourceStr) ? false : !string.IsNullOrEmpty(splitStr)))
			{
				strArrays = new string[0];
			}
			else if (sourceStr.IndexOf(splitStr) == -1)
			{
				strArrays = new string[] { sourceStr };
			}
			else if (splitStr.Length != 1)
			{
				strArrays = Regex.Split(sourceStr, Regex.Escape(splitStr), RegexOptions.IgnoreCase);
			}
			else
			{
				char[] chrArray = new char[] { splitStr[0] };
				strArrays = sourceStr.Split(chrArray);
			}
			return strArrays;
		}

		public static string[] SplitString(string sourceStr)
		{
			return StringHelper.SplitString(sourceStr, ",");
		}

		public static string SubString(string sourceStr, int startIndex, int length)
		{
			string str;
			if (string.IsNullOrEmpty(sourceStr))
			{
				str = "";
			}
			else
			{
				str = (sourceStr.Length < startIndex + length ? sourceStr.Substring(startIndex) : sourceStr.Substring(startIndex, length));
			}
			return str;
		}

		public static string SubString(string sourceStr, int length)
		{
			return StringHelper.SubString(sourceStr, 0, length);
		}

		public static string Trim(string sourceStr, string trimStr)
		{
			return StringHelper.Trim(sourceStr, trimStr, true);
		}

		public static string Trim(string sourceStr, string trimStr, bool ignoreCase)
		{
			string empty;
			if (string.IsNullOrEmpty(sourceStr))
			{
				empty = string.Empty;
			}
			else if (!string.IsNullOrEmpty(trimStr))
			{
				if (sourceStr.StartsWith(trimStr, ignoreCase, CultureInfo.CurrentCulture))
				{
					sourceStr = sourceStr.Remove(0, trimStr.Length);
				}
				if (sourceStr.EndsWith(trimStr, ignoreCase, CultureInfo.CurrentCulture))
				{
					sourceStr = sourceStr.Substring(0, sourceStr.Length - trimStr.Length);
				}
				empty = sourceStr;
			}
			else
			{
				empty = sourceStr;
			}
			return empty;
		}

		public static string TrimEnd(string sourceStr, string trimStr)
		{
			return StringHelper.TrimEnd(sourceStr, trimStr, true);
		}

		public static string TrimEnd(string sourceStr, string trimStr, bool ignoreCase)
		{
			string empty;
			if (!string.IsNullOrEmpty(sourceStr))
			{
				empty = ((string.IsNullOrEmpty(trimStr) ? false : sourceStr.EndsWith(trimStr, ignoreCase, CultureInfo.CurrentCulture)) ? sourceStr.Substring(0, sourceStr.Length - trimStr.Length) : sourceStr);
			}
			else
			{
				empty = string.Empty;
			}
			return empty;
		}

		public static string TrimStart(string sourceStr, string trimStr)
		{
			return StringHelper.TrimStart(sourceStr, trimStr, true);
		}

		public static string TrimStart(string sourceStr, string trimStr, bool ignoreCase)
		{
			string empty;
			if (!string.IsNullOrEmpty(sourceStr))
			{
				empty = ((string.IsNullOrEmpty(trimStr) ? false : sourceStr.StartsWith(trimStr, ignoreCase, CultureInfo.CurrentCulture)) ? sourceStr.Remove(0, trimStr.Length) : sourceStr);
			}
			else
			{
				empty = string.Empty;
			}
			return empty;
		}
        public static string getUniqueId(int iReqLength)
        {
            StringBuilder str = new StringBuilder();
            Random random = new Random();
            for (int i = 0; i < iReqLength; i++)
            {
                str.Append(digitArray[random.Next(digitArray.Length)]);
            }
            return str.ToString();
        }
        public static string getSNCode()
        {
            Random rd = new Random();            
            double d = rd.NextDouble();
            int i = (int)(d * 1000);
            DateTime nowDate = DateTime.Now;
            string sn = string.Format("{0}{1}{2}", nowDate.Year + 1900, nowDate.ToString("MMddHHmmss"), i);
            return sn;
        }
        /// <summary>
        /// 将排序（field1+,field2-）转成Json格式
        /// </summary>
        /// <param name="sort"></param>
        /// <returns></returns>
        public static string convertSortToJson(string sort)
        {
            try
            {
                string json = string.Empty;
                int order = 1;
                if (!string.IsNullOrEmpty(sort))
                {
                    var sortArray = sort.TrimEnd(',').Split(',');
                    if (sortArray.Length > 0)
                    {
                        json = "{";
                        foreach (string item in sortArray)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                if (item.Substring(item.Length - 1, 1) == "-")
                                {
                                    order = -1;
                                }
                                else
                                {
                                    order = 1;
                                }
                                string field = item.Substring(0, item.Length - 1);
                                json += "\"" + field + "\":" + order + ",";
                            }
                        }
                        json = json.TrimEnd(',');
                        json += "}";
                    }
                }
                return json;
            }
            catch(Exception ex)
            {
                Logger.Log(ex);
                return "";
            }
            
        }
        /// <summary>
        /// 将RequestBody参数转换成Json字符串
        /// </summary>
        /// <param name="query">格式：name=xiaopan&age=10</param>
        /// <returns>{name:"xiaopan",age:10}</returns>
        public static string queryToJson(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    var collect = HttpUtility.ParseQueryString(query);
                    var dict = collect.AllKeys.ToDictionary(k => k, k => collect[k]);
                    //Dictionary<string, object> result = new Dictionary<string, object>();
                    //collect.AllKeys.ToList().ForEach(s => {
                    //    result.Add(s, collect[s]);
                    //});
                    var jss = new JavaScriptSerializer();
                    return jss.Serialize(dict);
                }
                else
                {
                    return string.Empty;
                }
            }
            catch(Exception ex)
            {
                Logger.Log(ex);
                return "";
            }                     
        }

        private static string[] digitArray = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "B", "C", "D", "F", "G", "H", "J", "K", "L", "M", "N",
            "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z" };

    }
}