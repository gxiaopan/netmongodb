using System;
using System.Collections.Generic;

namespace NetMongo.Helper
{
    public class TypeHelper
    {
        public TypeHelper()
        {
        }

        public static bool ObjectToBool(object o, bool defaultValue)
        {
            bool flag;
            flag = (o == null ? defaultValue : TypeHelper.StringToBool(o.ToString(), defaultValue));
            return flag;
        }

        public static bool ObjectToBool(object o)
        {
            return TypeHelper.ObjectToBool(o, false);
        }

        public static DateTime ObjectToDateTime(object o, DateTime defaultValue)
        {
            DateTime dateTime;
            dateTime = (o == null ? defaultValue : TypeHelper.StringToDateTime(o.ToString(), defaultValue));
            return dateTime;
        }

        public static DateTime ObjectToDateTime(object o)
        {
            return TypeHelper.ObjectToDateTime(o, DateTime.Now);
        }

        public static decimal ObjectToDecimal(object o, decimal defaultValue)
        {
            decimal num;
            num = (o == null ? defaultValue : TypeHelper.StringToDecimal(o.ToString(), defaultValue));
            return num;
        }

        public static decimal ObjectToDecimal(object o)
        {
            return TypeHelper.ObjectToDecimal(o, new decimal(0));
        }

        public static int ObjectToInt(object o, int defaultValue)
        {
            int num;
            num = (o == null ? defaultValue : TypeHelper.StringToInt(o.ToString(), defaultValue));
            return num;
        }

        public static int ObjectToInt(object o)
        {
            return TypeHelper.ObjectToInt(o, 0);
        }

        public static bool StringToBool(string s, bool defaultValue)
        {
            bool flag;
            if (!(s == "false"))
            {
                flag = (!(s == "true") ? defaultValue : true);
            }
            else
            {
                flag = false;
            }
            return flag;
        }

        public static DateTime StringToDateTime(string s, DateTime defaultValue)
        {
            DateTime dateTime;
            DateTime dateTime1;
            if (!string.IsNullOrWhiteSpace(s))
            {
                if (DateTime.TryParse(s, out dateTime))
                {
                    dateTime1 = dateTime;
                    return dateTime1;
                }
            }
            dateTime1 = defaultValue;
            return dateTime1;
        }

        public static DateTime StringToDateTime(string s)
        {
            return TypeHelper.StringToDateTime(s, DateTime.Now);
        }

        /// <summary>  
        /// 将c# DateTime时间格式转换为Unix时间戳格式  
        /// </summary>  
        /// <param name="time">时间</param>  
        /// <returns>long</returns>  
        public static long ConvertDateTimeToInt(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            long t = (time.Ticks - startTime.Ticks) / 10000;   //除10000调整为13位      
            return t;
        }


        public static decimal StringToDecimal(string s, decimal defaultValue)
        {
            decimal num;
            decimal num1;
            if (!string.IsNullOrWhiteSpace(s))
            {
                if (decimal.TryParse(s, out num))
                {
                    num1 = num;
                    return num1;
                }
            }
            num1 = defaultValue;
            return num1;
        }

        public static decimal StringToDecimal(string s)
        {
            return TypeHelper.StringToDecimal(s, new decimal(0));
        }

        public static int StringToInt(string s, int defaultValue)
        {
            int num;
            int num1;
            if (!string.IsNullOrWhiteSpace(s))
            {
                if (int.TryParse(s, out num))
                {
                    num1 = num;
                    return num1;
                }
            }
            num1 = defaultValue;
            return num1;
        }

        public static long StringToLong(string s, long defaultValue)
        {
            long num;
            long num1;
            if (!string.IsNullOrWhiteSpace(s))
            {
                if (long.TryParse(s, out num))
                {
                    num1 = num;
                    return num1;
                }
            }
            num1 = defaultValue;
            return num1;
        }


        public static int StringToInt(string s)
        {
            return TypeHelper.StringToInt(s, 0);
        }
        public static long StringToLong(string s)
        {
            return TypeHelper.StringToInt(s, 0);
        }

        public static bool ToBool(string s)
        {
            return TypeHelper.StringToBool(s, false);
        }

        public static List<long> ToList(string str, char chr)
        {
            List<long> result = new List<long>();
            long temp = 0;
            string[] arr = str.Split(chr);
            for (int i = 0; i < arr.Length; i++)
            {
                temp = 0;
                long.TryParse(arr[i], out temp);
                result.Add(temp);
            }
            return result;
        }
    }
}