﻿using NetMongo.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace NetMongo.IService
{
    public interface IUserServices
    {
        User Get(string Id);
        User Get(Expression<Func<User, bool>> exp);
        List<User> Search(int Skip, int Limit, string WhereJson = "", string OrderJson = "");
        List<User> Search(int Skip, int Limit, Expression<Func<User, bool>> expression = null, string OrderJson = "");
        bool Add(User model);
        bool Update(User model);
        bool Update(string Id, Dictionary<string, object> fieldparas);
        bool Update(List<User> model);
        bool Delete(string Id);
        void Delete(User model);
        void Delete(Expression<Func<User, bool>> exp);
        void DeleteAll();
        void DeleteAll(List<User> model);
    }
}
