﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NetMongo.Startup))]
namespace NetMongo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
