﻿using NetMongo.Helper;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace NetMongo
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //日记注册
            log4net.Config.XmlConfigurator.Configure();
            Logger.Log("注册log4net完成");
        }
    }
}
