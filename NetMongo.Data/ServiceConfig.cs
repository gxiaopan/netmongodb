﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace NetMongo.Data
{
    public class ServiceConfig
    {
        [XmlArray, XmlArrayItem("Item")]
        public List<MongodbConfig> mongodbs { get; set; }
    }

    [XmlRoot]
    public class MongodbConfig
    {
        [XmlAttribute("dbName")]
        public string DBName { get; set; }
        [XmlAttribute("host")]
        public string Host { get; set; }

        [XmlAttribute("authMechanism")]
        public string AuthMechanism { get; set; }

        [XmlAttribute("userName")]
        public string UserName { get; set; }

        [XmlAttribute("password")]
        public string Password { get; set; }
    }
}
