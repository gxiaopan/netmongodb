﻿using NetMongo.Data;
using NetMongo.Entity;
using NetMongo.IService;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace NetMongo.Service
{
    public class UserServices : MongodbBase<User>, IUserServices
    {
        public UserServices()
        {
            base.Init("NetMongoDB");
        }

    }
}
